import { render } from "react-dom";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import './styles.css'

import Navbar from "./src/components/navbar";

import CreateProfile from "./src/pages/CreateProfile";
import ViewItem from "./src/pages/Customer/viewItems"
import ViewCart from "./src/pages/Customer/viewCart";
import Purchase from "./src/pages/Customer/purchase";
import AddItem from "./src/pages/Trader/addItem";
import AddPromotion from "./src/pages/Trader/addPromotions";
import EditItem from "./src/pages/Trader/editItem";
import ViewCustomer from "./src/pages/Trader/viewCustomer";
import ViewInventory from "./src/pages/Trader/viewInventory";
import ViewPromotions from "./src/pages/Trader/viewPromotions";
import Wishlist from "./src/pages/Customer/wishlist";
const rootElement = document.getElementById("app");
render(
  <BrowserRouter>
    <Navbar />
    <Routes>
      <Route path="/" element={<CreateProfile />} />
      <Route path="/items" element={<ViewItem />} />
      <Route path="/wishlist" element={<Wishlist />} />
      <Route path="/cart" element={<ViewCart />} />
      <Route path="/purchase" element={< Purchase />} />

      <Route path="/addItem" element={< AddItem />} />
      <Route path="/addPromotion" element={< AddPromotion />} />
      <Route path="/editItem" element={< EditItem />} />
      <Route path="/customers" element={< ViewCustomer />} />
      <Route path="/viewInventory" element={< ViewInventory />} />
      <Route path="/promotion" element={< ViewPromotions />} />
    </Routes>
  </BrowserRouter>, rootElement)