const mongoose = require('mongoose');

const dbClient = () => {
    const URI = process.env.MONGODB_URI;

    mongoose.connect(URI, () => {
        console.log("database connection success!")
    })
}

module.exports = { dbClient }