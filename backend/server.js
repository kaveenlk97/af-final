const Koa = require('koa')
const KoaRouter = require('koa-router')
const cors = require('@koa/cors')
const bodyParser = require('koa-bodyparser')
const json = require('koa-json')
const { dbClient } = require('./db/dbClient')

require('dotenv').config()

const app = new Koa()
const Router = new KoaRouter()

app.use(cors())
app.use(bodyParser())
app.use(json())
app.use(Router.routes()).use((Router.allowedMethods()))

app.listen(4000, () => {
    dbClient()
    console.log("Server is running on port 4000")
})
